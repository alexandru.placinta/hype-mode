import React, { Component, Fragment } from 'react';
import { connect } from 'dva';
import axios from 'axios';
import { Button, Form, Icon, Input, Modal, Popconfirm, Table, Upload, message } from 'antd';
import update from 'immutability-helper';

const { Column, ColumnGroup } = Table;
const FormItem = Form.Item;
const { TextArea } = Input;

const API_HOST = 'http://localhost:5000/';

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  // const isJPG = file.type === 'image/jpeg';
  // if (!isJPG) {
  //   message.error('You can only upload JPG file!');
  // }
  // const isLt2M = file.size / 1024 / 1024 < 2;
  // if (!isLt2M) {
  //   message.error('Image must smaller than 2MB!');
  // }
  // return isJPG && isLt2M;
  return true;
}

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};

class KeywordForm extends Component {
  state = {
    imageLoading: false,
  };

  handleImageChange = info => {
    // if (info.file.status === 'uploading') {
    //   this.setState({ imageLoading: true });
    //   return;
    // }
    // if (info.file.status === 'done') {
    // Get this url from response in real world.
    getBase64(info.file.originFileObj, imageUrl =>
      this.setState({
        imageUrl,
        imageLoading: false,
      })
    );
    // }
  };

  render() {
    const { visible, confirmLoading, onCancel, onOk, form } = this.props;
    const { getFieldDecorator } = form;

    const uploadButton = (
      <div>
        <Icon type={this.state.loading ? 'loading' : 'plus'} />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    const imageUrl = this.state.imageUrl;

    return (
      <Modal
        title="Title"
        visible={visible}
        onOk={onOk}
        confirmLoading={confirmLoading}
        onCancel={onCancel}
      >
        <Form layout="horizontal">
          <FormItem label="Keyword" {...formItemLayout}>
            {getFieldDecorator('keyword', {
              rules: [{ required: true, message: 'Please input the title of collection!' }],
            })(<TextArea autosize />)}
          </FormItem>
          <FormItem label="Image" {...formItemLayout}>
            {getFieldDecorator('image')(
              <Upload
                listType="picture-card"
                className="avatar-uploader"
                showUploadList={false}
                beforeUpload={beforeUpload}
                onChange={this.handleImageChange}
              >
                {imageUrl ? (
                  <img style={{ width: '100%' }} src={imageUrl} alt="avatar" />
                ) : (
                  uploadButton
                )}
              </Upload>
            )}
          </FormItem>
        </Form>
      </Modal>
    );
  }
}
const WrappedKeywordForm = Form.create()(KeywordForm);

@connect(({ keywords, loading }) => ({
  keywords,
  loading: loading.effects['chart/fetch'],
}))
export default class Analysis extends Component {
  state = {
    keywordImageAssociations: {
      data: [],
      pagination: {
        pageSize: 10,
        current: 1,
      },
      loading: false,
    },
    keywordCreation: {
      visible: false,
      confirmLoading: false,
    },
  };

  handleDelete = id => {
    this.setState({
      keywordImageAssociations: update(this.state.keywordImageAssociations, {
        loading: { $set: true },
      }),
    });
    console.log(this.state.keywordImageAssociations.pagination);
    axios.delete(`${API_HOST}keywordImage/${id}`).then(res => {
      this.fetch({
        size: this.state.keywordImageAssociations.pagination.pageSize,
        page: this.state.keywordImageAssociations.pagination.current,
        last_existing: true,
      });
    });
  };

  handleTableChange = (pagination, filters, sorter) => {
    this.fetch({
      size: this.state.keywordImageAssociations.pagination.pageSize,
      page: pagination.current,
      last_existing: true,
    });
  };

  fetch = (params = {}) => {
    this.setState({
      keywordImageAssociations: update(this.state.keywordImageAssociations, {
        loading: { $set: true },
      }),
    });
    axios.post(`${API_HOST}keywordImage/search`, params).then(res => {
      const result = res.data;
      console.log(result);
      this.setState({
        keywordImageAssociations: update(this.state.keywordImageAssociations, {
          data: { $set: result.content },
          pagination: {
            total: { $set: result.totalElements },
            current: { $set: result.number + 1 },
          },
          loading: { $set: false },
        }),
      });
    });
  };

  showKeywordCreationModal = () => {
    this.setState({
      keywordCreation: update(this.state.keywordCreation, {
        visible: { $set: true },
      }),
    });
  };

  handleKeywordCreationModalCancel = () => {
    this.setState({
      keywordCreation: update(this.state.keywordCreation, {
        visible: { $set: false },
      }),
    });
  };

  handleKeywordCreationModalOk = () => {
    const { form } = this.keywordFormRef.props;

    this.setState({
      keywordCreation: update(this.state.keywordCreation, {
        confirmLoading: { $set: true },
      }),
    });
    form.validateFields((err, values) => {
      if (err) {
        this.setState({
          keywordCreation: update(this.state.keywordCreation, {
            confirmLoading: { $set: false },
          }),
        });
        return;
      }

      console.log('Received values of form: ', values);
      const bodyFormData = new FormData();
      bodyFormData.append('keyword', values.keyword);
      bodyFormData.append('image', values.image.file.originFileObj);
      axios
        .post(`${API_HOST}keywordImage`, bodyFormData, {
          headers: {
            'Content-Type': 'multipart/form-data',
          },
        })
        .then(res => {
          this.setState({
            keywordCreation: update(this.state.keywordCreation, {
              confirmLoading: { $set: false },
            }),
          });
          this.setState({
            keywordCreation: update(this.state.keywordCreation, {
              visible: { $set: false },
            }),
          });
          message.success('Keyword cu emoticon a fost creat cu succes!');
          form.resetFields();
          this.fetch({
            size: this.state.keywordImageAssociations.pagination.pageSize,
            page: 1,
          });
        });
    });
  };

  componentDidMount() {
    this.fetch({
      size: this.state.keywordImageAssociations.pagination.pageSize,
      page: 1,
    });
  }

  componentWillUnmount() {}

  saveKeywordFormRef = keywordFormRef => {
    this.keywordFormRef = keywordFormRef;
  };

  render() {
    const { rangePickerValue, salesType, currentTabKey } = this.state;
    const { chart, loading } = this.props;

    return (
      <Fragment>
        <Button onClick={this.showKeywordCreationModal} type="primary" style={{ marginBottom: 16 }}>
          Add keyword
        </Button>
        <Table
          rowKey={record => record.id}
          dataSource={this.state.keywordImageAssociations.data}
          pagination={this.state.keywordImageAssociations.pagination}
          loading={this.state.keywordImageAssociations.loading}
          onChange={this.handleTableChange}
        >
          <Column title="ID" dataIndex="id" />
          <Column title="Keyword" dataIndex="keyword" />
          <Column
            title="Icon"
            dataIndex="imageRelativePath"
            render={url => <img width="50" src={url} />}
          />
          <Column
            title="Actions"
            render={(text, record) =>
              this.state.keywordImageAssociations.data.length >= 1 ? (
                <Popconfirm
                  title="Sure to delete?"
                  cancelText={'Cancel'}
                  okText={'Confirm'}
                  onConfirm={() => this.handleDelete(record.id)}
                >
                  <Button type="danger" style={{ marginBottom: 16 }}>
                    Delete
                  </Button>
                </Popconfirm>
              ) : null
            }
          />
        </Table>
        <WrappedKeywordForm
          wrappedComponentRef={this.saveKeywordFormRef}
          title="Title"
          visible={this.state.keywordCreation.visible}
          onOk={this.handleKeywordCreationModalOk}
          confirmLoading={this.state.keywordCreation.confirmLoading}
          onCancel={this.handleKeywordCreationModalCancel}
        />
      </Fragment>
    );
  }
}
