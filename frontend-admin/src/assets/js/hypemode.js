function replaceNodeRecursive(node, key, image) {
  if (node.nodeName == "#text") {
    var text = node.nodeValue;
    var splitted_text = text.split(key);
    console.log(node.parentNode);
    if (splitted_text.length == 1) return;

    for (var i = 0; i < splitted_text.length; i++) {
      var textNode = document.createTextNode(splitted_text[i]);
      node.parentNode.insertBefore(textNode, node);
      if (i < splitted_text.length - 1) {
        var imgNode = document.createElement('img');
        imgNode.height = 30;
        imgNode.src = "data:image/jpg;base64, " + image;
        node.parentNode.insertBefore(imgNode, node);
      }
    }
    node.remove();
  }
  ;
  node.childNodes.forEach(function (childNode) {
    replaceNodeRecursive(childNode, key, image);
  })
}

function hypemode(classesToConvert) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
    if (this.readyState == 4 && this.status == 200) {
      // Typical action to be performed when the document is ready:
      var keywords = JSON.parse(xhttp.responseText);
      var keyToEmojiMap = {};
      for (var i = 0; i < keywords.length; i++) {
        keyToEmojiMap[keywords[i].keyword] = keywords[i].imageRelativePath;
      }

      for (var i = 0; i < classesToConvert.length; i++) {
        var htmlClass = classesToConvert[i];
        var elements = document.getElementsByClassName(htmlClass);
        for (var j = 0; j < elements.length; j++) {
          var element = elements[j];
          for (var key in keyToEmojiMap) {
            replaceNodeRecursive(element, key, keyToEmojiMap[key]);
          }

        }
      }
    }
  };
  xhttp.open("GET", "http://hypemode.agora.md:5000/keywordImage", true);
  xhttp.send();
}

