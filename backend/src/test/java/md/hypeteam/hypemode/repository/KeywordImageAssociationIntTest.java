package md.hypeteam.hypemode.repository;

import md.hypeteam.hypemode.model.KeywordImageAssociation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.transaction.Transactional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class KeywordImageAssociationIntTest {
    @Autowired
    private KeywordImageAssociationRepository keywordImageAssociationRepository;

    @Test
    public void findAllWithPagination() {
        KeywordImageAssociation keywordImage = new KeywordImageAssociation();
        keywordImage.setKeyword("1");
        KeywordImageAssociation keywordImage2 = new KeywordImageAssociation();
        keywordImage2.setKeyword("2");
        KeywordImageAssociation keywordImage3 = new KeywordImageAssociation();
        keywordImage3.setKeyword("3");
        keywordImageAssociationRepository.save(keywordImage);
        keywordImageAssociationRepository.save(keywordImage2);
        keywordImageAssociationRepository.save(keywordImage3);

        Page<KeywordImageAssociation> result = keywordImageAssociationRepository.findAll(PageRequest.of(0, 2));
        assertEquals(2, result.getContent().size());
        assertEquals("1", result.getContent().get(0).getKeyword());
    }
}
