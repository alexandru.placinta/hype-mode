package md.hypeteam.hypemode.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "keyword_image_association")
public @Data class KeywordImageAssociation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "keyword")
    private String keyword;

    @Lob
    @Column(name = "imageRelativePath")
    private String imageRelativePath;
}
