package md.hypeteam.hypemode.dto;

import lombok.Data;

@Data
public class PaginationDTO {
    public int page;
    public int size;
    public boolean last_existing;
}
