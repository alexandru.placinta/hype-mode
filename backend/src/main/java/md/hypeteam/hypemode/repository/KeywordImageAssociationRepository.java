package md.hypeteam.hypemode.repository;

import md.hypeteam.hypemode.model.KeywordImageAssociation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KeywordImageAssociationRepository extends JpaRepository<KeywordImageAssociation, Long> {
}
