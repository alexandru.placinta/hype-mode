package md.hypeteam.hypemode.controller;

import md.hypeteam.hypemode.dto.PaginationDTO;
import md.hypeteam.hypemode.model.KeywordImageAssociation;
import md.hypeteam.hypemode.repository.KeywordImageAssociationRepository;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;


@RestController
@RequestMapping(value = "/keywordImage")
public class KeywordImageAssociationController {
    @Value("${image_path:/home/hypemode/images/}")
    private String imagePath;
    @Value("${image_url:http://hypemode.agora.md:1234/}")
    private String imageUrl;

    @Autowired
    private KeywordImageAssociationRepository keywordImageAssociationRepository;
//-Dimage_path=/home/hypemode/images/ -Dimage_url=http://hypemode.agora.md:1234/
    @GetMapping
    public ResponseEntity<Object> getadssdsadas() {
        List<KeywordImageAssociation> all = keywordImageAssociationRepository.findAll();
        all.forEach(keywordImageAssociation -> {
            keywordImageAssociation.setImageRelativePath(imageUrl + keywordImageAssociation.getImageRelativePath());


        });
        return ResponseEntity.ok(all);
    }

    @PostMapping(value = "search")
    public ResponseEntity<Object> getKeywordsWithPagination(@RequestBody PaginationDTO pagination) {
        if (pagination.size > 50) return new ResponseEntity<>(HttpStatus.BAD_REQUEST);


        int totalElements = (int) keywordImageAssociationRepository.count();
        int lastPage = (int) Math.ceil((float) totalElements / (float) pagination.size);
        if (lastPage < pagination.page) {
            pagination.page = lastPage;
            if (pagination.page == 0) {
                pagination.page = 1;
            }
        }

        Page<KeywordImageAssociation> result = keywordImageAssociationRepository.findAll(PageRequest.of(pagination.page - 1, pagination.size,
                Sort.Direction.DESC, "id"));
        result.getContent().forEach(keywordImageAssociation -> {
            keywordImageAssociation.setImageRelativePath(imageUrl + keywordImageAssociation.getImageRelativePath());
        });
        return ResponseEntity.ok(result);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Object> deleteKeyword(
            @PathVariable("id") Long id
    ) {

        keywordImageAssociationRepository.deleteById(id);
        return ResponseEntity.ok(id);
    }

    @PostMapping
    public ResponseEntity<Object> createKeyword(
            @RequestParam(value = "keyword", defaultValue = "1") String keyword,
            @RequestParam("image") MultipartFile image
    ) throws IOException {
        String imageName = "i" + String.valueOf(new Date().getTime()) + "." + FilenameUtils.getExtension(image.getOriginalFilename());
        image.transferTo(new File(imagePath + imageName));
        KeywordImageAssociation keywordImageAssociation = new KeywordImageAssociation();
        keywordImageAssociation.setKeyword(keyword);
        keywordImageAssociation.setImageRelativePath(imageName);
        this.keywordImageAssociationRepository.save(keywordImageAssociation);
        return ResponseEntity.ok(keywordImageAssociation);
    }

}

