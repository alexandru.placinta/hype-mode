package md.hypeteam.hypemode;

import md.hypeteam.hypemode.model.KeywordImageAssociation;
import md.hypeteam.hypemode.repository.KeywordImageAssociationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.domain.PageRequest;

@SpringBootApplication
public class HypemodeApplication {

    @Autowired
    private static ApplicationContext appContext;

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(HypemodeApplication.class, args);
    }
}
